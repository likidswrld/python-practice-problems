# Planning

## Research

* [ ] Vocab
* [ ] Functions
* [ ] Methods

## Problem decomposition

* [ ] Input
* [ ] Output
* [ ] Examples
* [ ] Conditions (if)
* [ ] Iteration (loop)

## Problems

### 01 minimum_value

Add plans...

### 04 max_of_three
#if 1>= 2 and 3 return 1
else
#if 2>=1 and 3 return 2
else
#if 3>= 1 and 2 return 3

### 06 can_skydive

### continue to plan each
