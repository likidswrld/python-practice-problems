# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_squares(values):
   #create inital loop to check for empty list
    if len(values) ==  0:
        return None
    #take each number and square it .append  each square to an empty list
    squares_to_add = []
    added_squares= 0
    for number in values:
        squares_to_add.append(number*number)

    for square in  squares_to_add:
        added_squares= int(square) + added_squares
    return added_squares
print(sum_of_squares([5,7,4,2]))
