# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return ("None")
    max_item = values[0]
    for item in values:
        if item > max_item:
            max_item=item
    return max_item
print(max_in_list([123,675,3333,989808,1119191919]))
